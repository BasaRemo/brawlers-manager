//
//  AthleteProfileVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SACalendar.h"
#import "RSDFDatePickerView.h"
#import "VENSeparatorView.h"
#import "PaymentsHistoryVC.h"
#import "UIColor+FlatUI.h"
#import "Athletes.h"
#import "PaymentVC.h"
#import "Payment.h"

@interface AthleteProfileVC : UIViewController <UITableViewDataSource,UITableViewDelegate>

- (IBAction)addPayment:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet Athletes *athlete;
- (IBAction)controlIndexChanged:(UISegmentedControl *)sender;

@end
