//
//  AthletesVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootVC.h"
#import "StudentsCell.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"
#import "StudentInfoViewController.h"
#import "Athletes.h"
#import "AthleteProfileVC.h"

@interface AthletesVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *athletesArray;
    NSInteger index;
}

@property (weak, nonatomic) IBOutlet UITableView *athletesTable;
@property  NSString *selectedName;
@property  UIColor *selectedColor;
@property  NSString *parseClassName;

- (IBAction)addStudent:(id)sender;
- (IBAction)showMenu:(id)sender;

@end
