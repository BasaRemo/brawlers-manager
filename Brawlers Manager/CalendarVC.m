//
//  CalendarVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//
#import "CalendarVC.h"

#define NAVBAR_CHANGE_POINT 50
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface CalendarVC()< RSDFDatePickerViewDataSource,RSDFDatePickerViewDelegate>

@property (strong, nonatomic) NSArray *datesToMark;
@property (strong, nonatomic) NSDictionary *statesOfTasks;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) RSDFDatePickerView *datePickerView;
@property (strong, nonatomic) RSDFCustomDatePickerView *customDatePickerView;

//@property (strong, nonatomic) FSCalendar *calendar;
//@property (strong, nonatomic) FSCalendarHeader *calendarHeader;
@end

@implementation CalendarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNewCalendar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*********** CALENDAR ************/
-(void) setNewCalendar{
    
    //SACalendar *calendar = [[SACalendar alloc]initWithFrame:self.view.bounds];
    //[self.calendarView addSubview:calendar];
    
    //Normal Date Picker
    _datePickerView = [[RSDFDatePickerView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height-202)];
    _datePickerView.delegate = self;
    _datePickerView.dataSource = self;
    
    //Custom Date Picker
    _customDatePickerView = [[RSDFCustomDatePickerView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height-202)];
    _customDatePickerView.delegate = self;
    _customDatePickerView.dataSource = self;
    _customDatePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:_datePickerView];
    //[_datePickerView setHidden:YES];
    //[self scrollToToday];
}
-(void) scrollToToday{
    
    if (!self.datePickerView) {
        [self.datePickerView scrollToToday:YES];
    } else {
        [self.customDatePickerView scrollToToday:YES];
    }
}
// Returns YES if the date should be highlighted or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldHighlightDate:(NSDate *)date
{
    return YES;
}

// Returns YES if the date should be selected or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldSelectDate:(NSDate *)date
{
    return YES;
}

// Prints out the selected date.
- (void)datePickerView:(RSDFDatePickerView *)view didSelectDate:(NSDate *)date
{
    NSLog(@"%@", [date description]);
}

// Returns YES if the date should be marked or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldMarkDate:(NSDate *)date
{
    // The date is an `NSDate` object without time components.
    // So, we need to use dates without time components.
    
    //NSDate *date1 = [NSDate dateWithDay:14 month:02 year:2015];
    //NSDate *date2 = [NSDate dateWithDay:21 month:02 year:2015];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *todayComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDate *today = [calendar dateFromComponents:todayComponents];
    
    return [date isEqual:today];
}
/*********** Calendar End ************/

/*-(void) setCalendar{
 _calendar = [[FSCalendar alloc]initWithFrame:CGRectMake(0,40,self.view.frame.size.width,self.view.frame.size.height-202)];
 _calendar.dataSource = self;
 _calendar.delegate = self;
 //[_calendar setHidden:YES];
 _calendar.flow = FSCalendarFlowVertical;
 
 _calendarHeader = [[FSCalendarHeader alloc] initWithFrame:CGRectMake(0,0,_calendar.frame.size.width,30)];
 _calendarHeader.backgroundColor = [UIColor pomegranateColor];
 _calendar.header = _calendarHeader;
 [self.view addSubview:_calendar];
 [self.view addSubview:_calendarHeader];
 
 [[FSCalendar appearance] setWeekdayTextColor:[UIColor pomegranateColor]];
 [[FSCalendar appearance] setHeaderTitleColor:[UIColor whiteColor]];
 
 [[FSCalendar appearance] setEventColor:[UIColor turquoiseColor]];
 [[FSCalendar appearance] setSelectionColor:[UIColor greenSeaColor]];
 [[FSCalendar appearance] setHeaderDateFormat:@"MMMM yyyy"];
 [[FSCalendar appearance] setMinDissolvedAlpha:0.6];
 [[FSCalendar appearance] setTodayColor:[UIColor pomegranateColor]];
 [[FSCalendar appearance] setUnitStyle:FSCalendarUnitStyleCircle];
 
 _calendar.backgroundColor = [UIColor whiteColor];
 //[_calendarHeader setHidden:YES];
 
 }
 //**
 
 -(NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date{
 
 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
 [dateFormat setDateFormat:@"yyyy-MM-dd"];
 
 NSString *dateString = [dateFormat stringFromDate:date];
 //NSLog(@"date: %@",dateString);
 
 NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"◉"];
 [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
 
 if ([_attendanceArray containsObject:dateString]) {
 UILabel *myLabel = [[UILabel alloc]init];
 myLabel.text = [NSString stringWithFormat:@"●"];
 myLabel.backgroundColor = [UIColor emerlandColor];
 myLabel.textColor = [UIColor emerlandColor];
 myLabel.tintColor = [UIColor emerlandColor];
 
 return myLabel.text;
 }
 return @"";
 //◉,●,○,◌,◎,◍,😓,
 //🏃 🚶 💃 🚣 🏊 🏄 🏂 🎿 ⛄️ 🚴 🚵 🏇 ⛺️ 🎣 ⚽️ 🏀 🏈 ⚾️ 🎾 🏉 ⛳️ 🏆 🎽 🏁
 
 
 }
 - (BOOL)calendar:(FSCalendar *)calendarView hasEventForDate:(NSDate *)date
 {
 return TRUE;
 }
 -(BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date
 {
 return TRUE;
 }
 */
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
