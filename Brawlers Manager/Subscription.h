//
//  Subscription.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subscription : NSObject
@property (retain) NSString *type;
@property (retain) NSDate *expirationDate;
@end
