//
//  Athlete.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-11.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Athletes : PFObject <PFSubclassing>
+ (NSString *)parseClassName;
@property (retain) NSString *name;
@property (retain) NSString *cellPhone;
@property (retain) NSString *email;
@property (retain) NSString *facebook;
@property (retain) NSArray *attendance;

@property BOOL *sex;
@property int status;
@property int age;

@end
