//
//  AppDelegate.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-03.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UVConfig.h"
#import "UserVoice.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

