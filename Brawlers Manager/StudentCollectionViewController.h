//
//  StudentCollectionViewController.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-05.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"
#import "AtheleteCollecCell.h"

@interface StudentCollectionViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSMutableArray *studentsArray;
}
@property (weak, nonatomic) IBOutlet UIView *totalNbView;
@property (nonatomic) NSMutableArray *selectedStudents;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *attendentNb;
@end
