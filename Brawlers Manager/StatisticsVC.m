//
//  StatisticsVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "StatisticsVC.h"
#import "UIColor+FlatUI.h"

@interface StatisticsVC ()

@end

@implementation StatisticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[[UINavigationBar appearance] setBarTintColor:[UIColor pomegranateColor]];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.scrollView setShowsVerticalScrollIndicator:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:YES];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setScrollEnabled:YES];
    
    [self.navigationItem setTitle:@"Statistics"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    [self lineChart];
    [self circleChart];
    [self pieChart];
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 750);
}

-(void) lineChart {
    
    //For Line Chart
    PNLineChart * lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0,60.0, SCREEN_WIDTH, 190.0)];
    [lineChart setXLabels:@[@"Monday",@" Wednesday",@"Friday"]];
    
    // Line Chart No.1
    NSArray * data01Array = @[@60.1, @160.1, @126.4, @262.2, @186.2];
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNFreshGreen;
    data01.itemCount = lineChart.xLabels.count;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    // Line Chart No.2
    NSArray * data02Array = @[@20.1, @180.1, @26.4, @202.2, @126.2];
    PNLineChartData *data02 = [PNLineChartData new];
    data02.color = PNTwitterColor;
    data02.itemCount = lineChart.xLabels.count;
    data02.getData = ^(NSUInteger index) {
        CGFloat yValue = [data02Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    lineChart.chartData = @[data01, data02];
    [lineChart strokeChart];
    
    [self.scrollView addSubview:lineChart];
}
-(void) circleChart{
    
    //Chart 1
    PNCircleChart * circleChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(20,330.0, 120, 100.0)
                                                                 total:@100
                                                               current:@60
                                                             clockwise:YES];
    circleChart.backgroundColor = [UIColor clearColor];
    [circleChart setStrokeColor:PNGreen];
    [circleChart strokeChart];
    
    //Chart 2
    PNCircleChart * circleChart2 = [[PNCircleChart alloc] initWithFrame:CGRectMake(160,330.0, 120, 100.0)
                                                                 total:@100
                                                               current:@60
                                                             clockwise:YES];
    circleChart2.backgroundColor = [UIColor clearColor];
    [circleChart2 setStrokeColor:PNGreen];
    [circleChart2 strokeChart];
    
    //Chart 3
    PNCircleChart * circleChart3 = [[PNCircleChart alloc] initWithFrame:CGRectMake(260,330.0, 120, 100.0)
                                                                 total:@100
                                                               current:@60
                                                             clockwise:YES];
    circleChart3.backgroundColor = [UIColor clearColor];
    [circleChart3 setStrokeColor:PNGreen];
    [circleChart3 strokeChart];
    
    [self.scrollView addSubview:circleChart];
    [self.scrollView addSubview:circleChart2];
    //[self.view addSubview:circleChart3];
}

-(void) pieChart{
    
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:10 color:PNRed description:@"Owed"],
                       [PNPieChartDataItem dataItemWithValue:40 color:PNGreen description:@"Paid"],
                       ];
    
    PNPieChart *pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(80.0, 520.0, 150.0, 150.0) items:items];
    pieChart.descriptionTextColor = [UIColor whiteColor];
    pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    [pieChart strokeChart];
    [self.scrollView addSubview:pieChart];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
