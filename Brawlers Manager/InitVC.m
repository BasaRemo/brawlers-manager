//
//  InitVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-28.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "InitVC.h"
static NSString *identifier = @"initCell";
@interface InitVC ()
{
    NSMutableArray *itemsImage;
     NSMutableArray *itemsTitle;
}
@end

@implementation InitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    itemsImage = [NSMutableArray arrayWithObjects:@"boxing-96.png", @"reviewer-104.png",@"show_property-104.png", @"combo-96.png",nil];
    itemsTitle = [NSMutableArray arrayWithObjects:@"Athletes", @"Attendance", @"History", @"Statistics",nil];
    [self setApparence];
}

-(void) setApparence{
    
    [self.navigationController.navigationBar  setBarTintColor:[UIColor pomegranateColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationItem setTitle:@"Main Menu"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UINavigationBar appearance] setBarTintColor:[UIColor pomegranateColor]];
    
    // Code to hide the separtor between navbar and the view
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //[self.navigationController.navigationBar setShadowImage:[UIImage new]];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [itemsTitle count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *itemImageView = (UIImageView *)[cell viewWithTag:100];
    itemImageView.image = [UIImage imageNamed:[itemsImage objectAtIndex:indexPath.row]];
    
    UILabel *itemTitleLabel = (UILabel*)[cell viewWithTag:101];
     itemTitleLabel.text =[itemsTitle objectAtIndex:indexPath.row];
    
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            [self gotoAthletesListView];
            break;
        case 1:
            [self gotoAttendanceView];
            break;
        case 2:
            [self gotoHistoryView];
            break;
        case 3:
            [self gotoStatsView];
            break;
            
        default:
            break;
    }
}

-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
}

- (void)gotoAthletesListView{
    AthletesVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    [self.navigationController pushViewController:viewController animated:YES];
    //[self presentViewController:viewController animated:YES completion:nil];
}

- (void)gotoAttendanceView{
    StudentCollectionViewController*viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AttendanceController"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)gotoHistoryView{
    HistoryVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)gotoStatsView{
    StatisticsVC *viewController = [[StatisticsVC alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
