//
//  PaymentVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-17.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentVC : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *amountNumberField;

@end
