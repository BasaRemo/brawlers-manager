//
//  StatisticsVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"

@interface StatisticsVC : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
