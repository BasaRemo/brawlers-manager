//
//  AthleteInfoCell.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AthleteInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
- (IBAction)contactBtnTapped:(id)sender;

@end
