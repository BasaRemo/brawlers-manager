//
//  AtheleteCollecCell.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-23.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtheleteCollecCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *athleteImage;

-(void) setUserImageAndText:(NSString *) name;

@end
