//
//  AthleteInfoVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+FlatUI.h"

@interface AthleteInfoVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) UITableView *tableView;
@end
