//
//  AthletesVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AthletesVC.h"
#import "AFNetworkReachabilityManager.h"
#import "UserVoice.h"
@import AddressBook;

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

static NSString * const sampleDescription1 = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
static NSString * const sampleDescription2 = @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.";
static NSString * const sampleDescription3 = @"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.";

@interface AthletesVC ()

@end

@implementation AthletesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.athletesTable.allowsMultipleSelection = YES;
    self.parseClassName = @"Athletes";
    
    //[self queryFromParse];
    [self isInternetReachable];
    [_athletesTable reloadData];
    [self setApparence];
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    // Set Tab Bar Item
    //self.tabBarController.tabBar.backgroundColor = [UIColor pomegranateColor];
    //self.tabBarController.tabBar.tintColor = [UIColor whiteColor];
    //self.tabBarController.tabBar.barTintColor = [UIColor pomegranateColor];
    //[self.tabBarController.tabBar setTranslucent:NO];
}
- (void)setApparence
{
    //Nav Bar appearance
    [self.navigationItem setTitle:@"Athletes"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    // Code to hide the separtor between navbar and the view
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //[self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    //UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:nil];
    //UIBarButtonItem *addStudentBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:nil];
    
    //self.navigationItem.leftBarButtonItem = shareItem;
    //self.navigationItem.rightBarButtonItem = addStudentBtn;
}

-(BOOL) isInternetReachable
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSLog(@"Reachability changed: %@", AFStringFromNetworkReachabilityStatus(status));
        
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                // -- Reachable -- //
                NSLog(@"Reachable");
                [self queryFromParse];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                // -- Not reachable -- //
                NSLog(@"Not Reachable");
                [self queryFromLocalStore];
                break;
        }
        
    }];
    return NO;

}
- (PFQuery*) queryFromParse {
    
    PFQuery *query = [Athletes query];
    [query orderByDescending:@"createdAt"];
    //query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Online!");
            athletesArray = [[NSMutableArray alloc] initWithArray:objects];
            [PFObject unpinAllObjectsInBackgroundWithName:@"MyAthletes"];
            [PFObject pinAllInBackground:objects withName:@"MyAthletes"];
            [_athletesTable reloadData];
            //NSLog(@"Objects Retrieved! %@",studentsArray);
        }
    }];
    
    return query;
}
-(PFQuery*) queryFromLocalStore{
 
 PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
 [query fromLocalDatastore];
 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
 if (!error) {
     athletesArray = [[NSMutableArray alloc] initWithArray:objects];
     [_athletesTable reloadData];
 }
 }];
 
 return query;
 }
/*-(PFQuery*) queryFromCache{
 
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    [query fromLocalDatastore];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            athletesArray = [[NSMutableArray alloc] initWithArray:objects];
            //NSLog(@"Objects Retrieved! %@",studentsArray);
            
        }
        [_athletesTable reloadData];
    }];
    
    return query;
}*/

/*- (IBAction)addStudent:(id)sender {
    
    // Create PFObject with recipe information
    Athlete *athlete = [Athlete object];
    athlete.firstName = @"Jean";
    athlete.lastName = @"Paul";
    athlete.age = 25;
    athlete.sex = @"male";
    
    // Show progress
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Uploading";
    [hud show:YES];
    
    // Upload student to Parse
    [athlete saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [hud hide:YES];
        
        if (!error) {
            // Show success message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Complete" message:@"Successfully saved the student" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            // Notify table view to reload the athletes from Parse cloud
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
            [self queryFromParse];
            
            // Dismiss the controller
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}
*/
- (IBAction)showMenu:(id)sender {
    
    // Call this wherever you want to launch UserVoice
    [UserVoice presentUserVoiceInterfaceForParentViewController:self];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"showMenu" object:nil];
}

//*********************Setup table of folder names ************************

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [athletesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"studentsCell";
    
    StudentsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[StudentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    PFObject *tempObject = [athletesArray objectAtIndex:indexPath.row];
    NSString *name =[tempObject objectForKey:@"name"];
    cell.name.text = name;
    NSNumber *status = [tempObject objectForKey:@"status"];
    
    cell.statusBtn.backgroundColor = [UIColor clearColor];
    cell.statusBtn.cornerRadius = cell.statusBtn.bounds.size.width/2;
    [cell.statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // Change the student status btn color
    if ([status integerValue] == 0) {
        cell.statusBtn.buttonColor = [UIColor alizarinColor];
        //[cell.statusBtn setTitle:@"3" forState:UIControlStateNormal];
    }else if ([status integerValue] == 1){
        cell.statusBtn.buttonColor = [UIColor emerlandColor];
        //cell.statusBtn.hidden = true;
    }else if ([status integerValue] == 2){
        cell.statusBtn.buttonColor = [UIColor orangeColor];
        //cell.statusBtn.hidden = true;
    }
    [cell setUserImageAndText:name];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StudentsCell *tableViewCell = (StudentsCell*)[tableView cellForRowAtIndexPath:indexPath];
    _selectedName = tableViewCell.name.text;
    index = indexPath.row;
    NSLog(@"selected name: %@",_selectedName);
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StudentsCell *tableViewCell = (StudentsCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    [tableViewCell.attendanceBtn setImage:[UIImage imageNamed:@"icon-checkbox-selected-green-25x25.png"]];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StudentsCell *tableViewCell = (StudentsCell*)[tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell.attendanceBtn setImage:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"]];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self->athletesArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showProfile"])
    {
        Athletes *tempObject = [athletesArray objectAtIndex:index];

        tempObject.name =[tempObject objectForKey:@"name"];
        tempObject.email =[tempObject objectForKey:@"email"];
        tempObject.cellPhone =[tempObject objectForKey:@"cellphone"];
        tempObject.attendance = [tempObject objectForKey:@"attendances"];

        AthleteProfileVC* counterVC = [segue destinationViewController ];
        counterVC.athlete = tempObject;
        
        /*NSString *name =[tempObject objectForKey:@"name"];
         NSString *email =[tempObject objectForKey:@"email"];
         NSString *cellPhone =[tempObject objectForKey:@"cellphone"];
         NSArray *attendances = [tempObject objectForKey:@"attendances"];
         counterVC.name = name;
         counterVC.email = email;
         counterVC.cellPhone = cellPhone;
         counterVC.attendanceArray = attendances;
         */
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
