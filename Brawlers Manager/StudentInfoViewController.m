//
//  StudentInfoViewController.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-03.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "StudentInfoViewController.h"
#import "RSDFDatePickerView.h"
#import "RSDFCustomDatePickerView.h"
#import "HTHorizontalSelectionList.h"
#import "UIImageView+Letters.h"
 #import "RKTabView.h"
#import <MessageUI/MessageUI.h>
#import "CalendarVC.h"
#import "AthleteInfoVC.h"

#define NAVBAR_CHANGE_POINT 50

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface StudentInfoViewController () <HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate,RKTabViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) NSArray *datesToMark;
@property (strong, nonatomic) NSDictionary *statesOfTasks;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) RSDFDatePickerView *datePickerView;
@property (strong, nonatomic) RSDFCustomDatePickerView *customDatePickerView;

@property (nonatomic, strong) HTHorizontalSelectionList *detailsTabView;
@property (nonatomic, strong) NSArray *tabTitles;

@property (nonatomic, strong) UIView *paymentHistoryView;
@property (nonatomic, strong) PaymentsHistoryVC *paymentHistoryVC;
@property (nonatomic, strong) TSViewController *paymentVC;

@property (strong, nonatomic) AthleteInfoVC *athleteInfoVC;
@property (strong, nonatomic) UIView *athleteInfoView;
@property (strong, nonatomic) CalendarVC *calendarVC;
@property (strong, nonatomic) UIView *calendarView;
@property (strong, nonatomic) FSCalendarHeader *calendarHeader;
@property (strong, nonatomic) MFMailComposeViewController *globalMail;
@end

@implementation StudentInfoViewController

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    // Do any additional setup after loading the view.
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setDetailsTabBar];
    //[self setCustomTabBar2];
    [self setUserImage];
    [self setAthleteInfoView];
    //[self setNewCalendar];
    [self setCalendar];
    [self setNotifications];

    // Trick to hide Keyboard After finishing editing
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer: tapRec];
    
    UIBarButtonItem *addPayment = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:nil];
    self.navigationItem.rightBarButtonItem = addPayment;
    
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)tap:(UITapGestureRecognizer *)tapRec{
    [[self view] endEditing: YES];
}
-(void) setAttendaceArray{
    //attendanceArray = [[NSArray alloc] ini];
}
/*********** ATHLETE IMAGE ************/

-(void) setUserImage{
    
    //Nav Bar appearance
    self.title = @"Profile";
    _headerView.backgroundColor = [UIColor pomegranateColor];
    
    [_userImageView setImageWithString:self.athlete.name];
    _nameLabel.text = self.athlete.name;
    
    // Set corner radius
    _userImageView.layer.cornerRadius = CGRectGetHeight(_userImageView.frame) / 2;
    _userImageView.clipsToBounds = YES;
    
    // Add border
    _userImageView.layer.borderColor = [[UIColor colorWithWhite:0.84f alpha:1.0f] CGColor];
    _userImageView.layer.borderWidth = 1.5f;
    
    // Add shadow
    _userImageView.layer.shadowOffset = CGSizeMake(0, 0);
    _userImageView.layer.shadowRadius = 1.5;
    _userImageView.layer.shadowOpacity = 0.3;
}

/********** ATHLETE CALENDAR *********/
-(void) setCalendar{
    _calendarVC= [CalendarVC new];
    _calendarView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    [_calendarView addSubview:_calendarVC.view];
    [self.view addSubview:_calendarView];
    [_calendarView setHidden:YES];
}
/********** ATHLETE PAYMENTS HISTORY *********/

-(void) setPaymentHistory{
    _paymentHistoryVC= [self.storyboard instantiateViewControllerWithIdentifier:@"paymentHistoryVC"];
    _paymentHistoryVC.athleteName = _athlete.name;
    _paymentHistoryView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    [_paymentHistoryView addSubview:_paymentHistoryVC.view];
    [self.view addSubview:_paymentHistoryView];
    
}

/********** ATHLETE CONTACT INFO ************/

- (void)setAthleteInfoView
{
    _athleteInfoVC= [AthleteInfoVC new];
    _athleteInfoView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    [_athleteInfoView addSubview:_athleteInfoVC.view];
    [self.view addSubview:_athleteInfoView];
    
}

/************ TAB BAR ************/

-(void) setDetailsTabBar{
    
    self.detailsTabView = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0,120,self.view.frame.size.width,45)];
    self.detailsTabView.delegate = self;
    self.detailsTabView.dataSource = self;
    
    self.tabTitles = @[@"    Details  ",
                       @"   Calendar ",
                       @"   Payments "];
    
    [self.view addSubview:self.detailsTabView];
    
    [self.detailsTabView setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    self.detailsTabView.bottomTrimColor = [UIColor redColor];
    self.detailsTabView.selectionIndicatorColor = [UIColor redColor];
    
    //RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0,212,self.view.frame.size.width,44)];
    
    
}

/******** TAB BAR ********/

-(void) setCustomTabBar2{
    RKTabItem *tabItem1 = [RKTabItem createUsualItemWithImageEnabled:nil imageDisabled:nil];
    RKTabItem *tabItem2 = [RKTabItem createUsualItemWithImageEnabled:nil imageDisabled:nil];
    RKTabItem *tabItem3 = [RKTabItem createUsualItemWithImageEnabled:nil imageDisabled:nil];
    
    tabItem1.titleString = @"Information";
    tabItem2.titleString = @"Calendar";
    tabItem3.titleString = @"Payment";
    
    tabItem1.tabState = TabStateEnabled;
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0,120,self.view.frame.size.width,45)];
    tabView.delegate = self;
    tabView.backgroundColor = [UIColor whiteColor];
    //tabView.enabledTabBackgrondColor = [UIColor pomegranateColor];
    tabView.tabItems = @[tabItem1, tabItem2,tabItem3];
    
    tabView.titlesFontColor = [UIColor silverColor];
    tabView.titlesFont =[UIFont fontWithName:@"Helvetica" size:15];
    
    //tabView.horizontalInsets = HorizontalEdgeInsetsMake(130, 70);
    //tabView.drawSeparators = YES;
    tabView.darkensBackgroundForEnabledTabs = YES;
    [self.view addSubview:tabView];
}

#pragma mark - RKTabViewDelegate

- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(int)index tab:(RKTabItem *)tabItem {
    switch (index) {
        case 0:
            [self.tableView setHidden:NO];
            [_calendarView setHidden:YES];
            
            if (_paymentHistoryView)
                [_paymentHistoryView setHidden:YES];
            break;
            
        case 1:
            [self.tableView setHidden:YES];
            [_calendarView setHidden:NO];
            [_paymentHistoryView setHidden:YES];
            break;
            
        case 2:
            [self.tableView setHidden:YES];
            if (_paymentHistoryView){
                [_paymentHistoryView setHidden:NO];
                [_calendarView setHidden:YES];
            }
            else
                [self setPaymentHistory];
            break;
            
        default:
            [self.tableView setHidden:YES];
            [_calendarView setHidden:YES];
            break;
    }
    
}

- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(int)index tab:(RKTabItem *)tabItem {
        //NSLog(@"Tab № %d became disabled on tab view", index);
}


#pragma mark - HTHorizontalSelectionListDataSource Protocol Methods

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.tabTitles.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return self.tabTitles[index];
}

#pragma mark - HTHorizontalSelectionListDelegate Protocol Methods

- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    // update the view for the corresponding index
    //NSLog(@"Tab № %ld became enabled on tab view", (long)index);
    switch (index) {
        case 0:
            [self.tableView setHidden:NO];
            [_calendarView setHidden:YES];
            
            if (_paymentHistoryView)
                [_paymentHistoryView setHidden:YES];
            break;
            
        case 1:
            [self.tableView setHidden:YES];
            [_calendarView setHidden:NO];
            [_paymentHistoryView setHidden:YES];
            break;
            
        case 2:
            [self.tableView setHidden:YES];
            if (_paymentHistoryView){
                [_paymentHistoryView setHidden:NO];
                [_calendarView setHidden:YES];
            }
            else
                [self setPaymentHistory];
            break;
            
        default:
            [self.tableView setHidden:YES];
            [_calendarView setHidden:YES];
            break;
    }
    
}

/******** SEND EMAIL ***********/

// Notifications to change Background
-(void) setNotifications {
    
    // Set Observer to change background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendEmail:)name:@"sendEmail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendEmail:)name:@"call" object:nil];
}
-(void)cycleTheGlobalMailComposer
{
    // we are cycling the damned GlobalMailComposer... due to horrible iOS issue
    self.globalMail = nil;
    self.globalMail = [[MFMailComposeViewController alloc] init];
}
- (IBAction)sendEmail: (NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"sendEmail"]) {
        
        if ([MFMailComposeViewController canSendMail]) {
            
            [self cycleTheGlobalMailComposer];
            self.globalMail.mailComposeDelegate = self;
            [self.globalMail setSubject:@"Need My Money!!"];
            [self.globalMail setMessageBody:@"You Owed me some Money Brotha" isHTML:NO];
            [self.globalMail setToRecipients:@[self.athlete.email]];
            [self presentViewController:self.globalMail animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Son!"
                                                              message:@"Unable to mail. No email on this device?"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Fuck It"
                                                    otherButtonTitles:nil];
            
            [message show];
            [self cycleTheGlobalMailComposer];
        }

    }else if([[notification name] isEqualToString:@"call"]){
        
        NSString *phoneNumber =  self.athlete.cellPhone;
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

-(void) didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:^
     { [self cycleTheGlobalMailComposer]; }
     ];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"goToPayment"])
    {
        _paymentVC = [segue destinationViewController ];
        _paymentVC.paymentDelegate = self;
        _paymentVC.userName = _athlete.name;
    }
}

- (void)addNewPayment:(NSString*)amount{

    // Create PFObject with recipe information
    //Payment *payment = [Payment object];
    //payment.amount = amount;
    
    PFObject *payment = [PFObject objectWithClassName:@"Payment"];
    [payment setObject:amount forKey:@"amount"];
    [payment setObject:_athlete.name forKey:@"athleteName"];
    [payment setObject:@"monthly" forKey:@"subscription"];
    [payment setObject:[NSNumber numberWithBool:YES] forKey:@"complete"];
    
    // Upload student to Parse
    [payment pinInBackground];
    //[payment pinInBackgroundWithName:@"Payment"];
    [payment saveEventually];
     [_paymentHistoryVC queryFromLocalStore];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Complete" message:@"Payment Added!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    /*[payment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Show success message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Complete" message:@"Payment Added!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Add Payment :(" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];*/
}

- (IBAction)addPayment:(id)sender {
    PaymentVC *viewController = [[PaymentVC alloc] init];
    //[self presentViewController:viewController animated:YES completion:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
