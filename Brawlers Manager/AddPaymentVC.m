//
//  AddPaymentVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AddPaymentVC.h"
#import "GoogleWearAlertObjc.h"

@interface AddPaymentVC ()

@end

@implementation AddPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDecimalNumber *value = [[NSDecimalNumber alloc] initWithDouble:0.00];
    self.currencyField.decimalValue = value;
    self.currencyField.delegate = self;
    self.currencyField.locale = [NSLocale currentLocale];
    //self.currencyField.keyboardType  = UIKeyboardTypeNumbersAndPunctuation;
    
    _athleteName.text = _userName;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    recognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:recognizer];
    //[self.currencyField becomeFirstResponder];
    
    _userPicture.image = [UIImage imageNamed:@"user_male2-32.png"];
    
    _notes.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    _notes.layer.borderWidth=1.0;
    _notes.layer.cornerRadius = 8.0;
    _notes.layer.masksToBounds = YES;
    
    _confirmBtn.layer.cornerRadius = 6.0f;
    
    //self.navigationController.navigationBar.topItem.title = @"Payment";
 
}

- (void)handleTap
{
    [self.view endEditing:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Value: %@", ((JDFCurrencyTextField *)textField).decimalValue);
    NSLog(@"Text: %@", textField.text);
    _amount = textField.text;
}

- (IBAction)confirmTransaction:(id)sender {
    
    NSLog(@"Amout: %@",_amount );
    [self.view endEditing: YES];
    if (_amount!=NULL || _amount !=0) {
        [self.paymentDelegate addNewPayment:_amount];
        [[GoogleWearAlertObjc getInstance]prepareNotificationToBeShown:[[GoogleWearAlertViewObjc alloc]initWithTitle:@"Success" andImage:nil andWithType:Success andWithDuration:2.5 inViewController:self atPostion:Top canBeDismissedByUser:NO]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancelPayment:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
