//
//  AddPaymentVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDFCurrencyTextField.h"
#import "UIColor+FlatUI.h"
#import "FUIButton.h"

@protocol PaymentDelegate <NSObject>
-(void)addNewPayment:(NSString*)amount;
@end

@interface AddPaymentVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JDFCurrencyTextField *currencyField;
@property (nonatomic, assign) id <PaymentDelegate> paymentDelegate;

@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *amount;
@property (weak, nonatomic) IBOutlet UILabel *athleteName;
@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (weak, nonatomic) IBOutlet UITextView *notes;
- (IBAction)confirmTransaction:(id)sender;
@property (weak, nonatomic) IBOutlet FUIButton *confirmBtn;
- (IBAction)cancelPayment:(id)sender;

@end
