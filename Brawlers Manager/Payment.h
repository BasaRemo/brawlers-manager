//
//  Payment.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Subscription.h"

@interface Payment : PFObject<PFSubclassing>
+ (NSString *)parseClassName;
@property (retain) NSString *amount;
//@property (retain) Subscription *subscription;
//@property BOOL complete;
@end

