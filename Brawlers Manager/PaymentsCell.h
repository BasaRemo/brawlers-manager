//
//  PaymentsCell.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-26.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VENSeparatorTableViewCellProvider.h"
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"

@interface PaymentsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet VENSeparatorView *paymentView;
@property (weak, nonatomic) IBOutlet FUIButton *paidBtn;
@property (weak, nonatomic) IBOutlet UILabel *subscription;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet FUIButton *subscriptionStatus;

@end
