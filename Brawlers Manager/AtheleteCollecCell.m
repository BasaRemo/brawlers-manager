//
//  AtheleteCollecCell.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-23.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AtheleteCollecCell.h"
#import "UIImageView+Letters.h"

@implementation AtheleteCollecCell

-(void) setUserImageAndText:(NSString *) name{
    
    [_athleteImage setImageWithString:name];
    
    // Set corner radius
    _athleteImage.layer.cornerRadius = CGRectGetHeight(_athleteImage.frame) / 2;
    _athleteImage.clipsToBounds = YES;
    
    // Add border
    _athleteImage.layer.borderColor = [[UIColor colorWithWhite:0.84f alpha:1.0f] CGColor];
    _athleteImage.layer.borderWidth = 0.3f;
    
    // Add shadow
    _athleteImage.layer.shadowOffset = CGSizeMake(0, 0);
    _athleteImage.layer.shadowRadius = 1.5;
    _athleteImage.layer.shadowOpacity = 0.3;
}


@end
