//
//  StudentInfoViewController.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-03.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SACalendar.h"
#import "RSDFDatePickerView.h"
#import "VENSeparatorView.h"
#import "UIColor+FlatUI.h"
#import "Athletes.h"
#import "PaymentVC.h"
#import "Payment.h"

@interface StudentInfoViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

- (IBAction)addPayment:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet Athletes *athlete;

@end
