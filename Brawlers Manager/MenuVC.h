//
//  MenuVC.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-06.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
