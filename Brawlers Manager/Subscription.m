//
//  Subscription.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "Subscription.h"

@implementation Subscription
@dynamic type;
@dynamic expirationDate;
+ (NSString *)parseClassName {
    return @"Subscription.h";
}
@end
