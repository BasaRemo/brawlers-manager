//
//  AthleteProfileVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//
#import "AthleteProfileVC.h"
#import "StudentInfoViewController.h"
#import "RSDFDatePickerView.h"
#import "RSDFCustomDatePickerView.h"
#import "UIImageView+Letters.h"
#import "RKTabView.h"
#import <MessageUI/MessageUI.h>
#import "CalendarVC.h"
#import "AthleteInfoVC.h"
#import "AddPaymentVC.h"

#define NAVBAR_CHANGE_POINT 50

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface AthleteProfileVC () <RKTabViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) NSArray *datesToMark;
@property (strong, nonatomic) NSDictionary *statesOfTasks;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) RSDFDatePickerView *datePickerView;
@property (strong, nonatomic) RSDFCustomDatePickerView *customDatePickerView;

@property (nonatomic, strong) NSArray *tabTitles;

@property (nonatomic, strong) UIView *paymentHistoryView;
@property (nonatomic, strong) PaymentsHistoryVC *paymentHistoryVC;

@property (strong, nonatomic) AthleteInfoVC *athleteInfoVC;
@property (strong, nonatomic) UIView *athleteInfoView;
@property (strong, nonatomic) CalendarVC *calendarVC;
@property (strong, nonatomic) UIView *calendarView;
@property (strong, nonatomic) MFMailComposeViewController *globalMail;
@end

@implementation AthleteProfileVC

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    // Do any additional setup after loading the view.
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //[self setDetailsTabBar];
    //[self setCustomTabBar2];
    [self setUserImage];
    [self setAthleteInfoView];
    [self setCalendar];
    [self setNotifications];
    
    // Code to hide the separtor between navbar and the view
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Trick to hide Keyboard After finishing editing
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer: tapRec];
    
    UIBarButtonItem *addPayment = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:nil];
    self.navigationItem.rightBarButtonItem = addPayment;
    
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)tap:(UITapGestureRecognizer *)tapRec{
    [[self view] endEditing: YES];
}
-(void) setAttendaceArray{
    //attendanceArray = [[NSArray alloc] ini];
}
/*********** ATHLETE IMAGE ************/

-(void) setUserImage{
    
    //Nav Bar appearance
    self.title = @"Profile";
    //_headerView.backgroundColor = [UIColor blueColor];
    
    [_userImageView setImageWithString:self.athlete.name];
    _nameLabel.text = self.athlete.name;
    
    // Set corner radius
    _userImageView.layer.cornerRadius = CGRectGetHeight(_userImageView.frame) / 2;
    _userImageView.clipsToBounds = YES;
    
    // Add border
    _userImageView.layer.borderColor = [[UIColor colorWithWhite:0.84f alpha:1.0f] CGColor];
    _userImageView.layer.borderWidth = 1.5f;
    
    // Add shadow
    _userImageView.layer.shadowOffset = CGSizeMake(0, 0);
    _userImageView.layer.shadowRadius = 1.5;
    _userImageView.layer.shadowOpacity = 0.3;
}

/********** ATHLETE CALENDAR *********/
-(void) setCalendar{
    _calendarVC= [CalendarVC new];
    _calendarView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    [_calendarView addSubview:_calendarVC.view];
    [self.view addSubview:_calendarView];
    [_calendarView setHidden:YES];
}
/********** ATHLETE PAYMENTS HISTORY *********/

-(void) setPaymentHistory{
    _paymentHistoryVC= [self.storyboard instantiateViewControllerWithIdentifier:@"paymentHistoryVC"];
    _paymentHistoryVC.athleteName = _athlete.name;
    _paymentHistoryView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    
    _paymentHistoryVC.tableView.bounds = CGRectMake(0,165, 320, 320);
    /*CGRect frame = _paymentHistoryVC.tableView.frame;
    frame.size.height = 340;
    _paymentHistoryVC.tableView.frame = frame;*/
    
    [_paymentHistoryView addSubview:_paymentHistoryVC.view];
    [self.view addSubview:_paymentHistoryView];
    
}

/********** ATHLETE CONTACT INFO ************/

- (void)setAthleteInfoView
{
    _athleteInfoVC= [AthleteInfoVC new];
    _athleteInfoView = [[UIView alloc] initWithFrame:CGRectMake(0,165,self.view.frame.size.width,self.view.frame.size.height-202)];
    [_athleteInfoView addSubview:_athleteInfoVC.view];
    [self.view addSubview:_athleteInfoView];
    
}
- (IBAction)addPayment:(id)sender {
    AddPaymentVC *viewController = [[AddPaymentVC alloc] init];
    viewController.userName = _athlete.name;
    [self presentViewController:viewController animated:YES completion:nil];
    //[self.navigationController pushViewController:viewController animated:YES];
}

- (void)addNewPayment:(NSString*)amount{
    
    // Create PFObject with recipe information
    //Payment *payment = [Payment object];
    //payment.amount = amount;
    
    PFObject *payment = [PFObject objectWithClassName:@"Payment"];
    [payment setObject:amount forKey:@"amount"];
    [payment setObject:_athlete.name forKey:@"athleteName"];
    [payment setObject:@"monthly" forKey:@"subscription"];
    [payment setObject:[NSNumber numberWithBool:YES] forKey:@"complete"];
    
    // Upload student to Parse
    [payment pinInBackground];
    //[payment pinInBackgroundWithName:@"Payment"];
    [payment saveEventually];
    [_paymentHistoryVC queryFromLocalStore];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Complete" message:@"Payment Added!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}

- (IBAction)controlIndexChanged:(UISegmentedControl *)sender {
    
    long index = sender.selectedSegmentIndex;
    switch (index) {
        case 0:
            [self.athleteInfoView setHidden:NO];
            [_calendarView setHidden:YES];
            
            if (_paymentHistoryView)
                [_paymentHistoryView setHidden:YES];
            break;
            
        case 1:
            [self.athleteInfoView setHidden:YES];
            [_calendarView setHidden:NO];
            [_paymentHistoryView setHidden:YES];
            break;
            
        case 2:
            [self.athleteInfoView setHidden:YES];
            if (_paymentHistoryView){
                [_paymentHistoryView setHidden:NO];
                [_calendarView setHidden:YES];
            }
            else
                [self setPaymentHistory];
            break;
            
        default:
            [self.athleteInfoView setHidden:YES];
            [_calendarView setHidden:YES];
            break;
    }
}

/******** SEND EMAIL ***********/
// Notifications to change Background
-(void) setNotifications {
    
    // Set Observer to change background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendEmail:)name:@"sendEmail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendEmail:)name:@"call" object:nil];
}
-(void)cycleTheGlobalMailComposer
{
    // we are cycling the damned GlobalMailComposer... due to horrible iOS issue
    self.globalMail = nil;
    self.globalMail = [[MFMailComposeViewController alloc] init];
}
- (IBAction)sendEmail: (NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"sendEmail"]) {
        
        if ([MFMailComposeViewController canSendMail]) {
            
            [self cycleTheGlobalMailComposer];
            self.globalMail.mailComposeDelegate = self;
            [self.globalMail setSubject:@"Need My Money!!"];
            [self.globalMail setMessageBody:@"You Owed me some Money Brotha" isHTML:NO];
            [self.globalMail setToRecipients:@[self.athlete.email]];
            [self presentViewController:self.globalMail animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Son!"
                                                              message:@"Unable to mail. No email on this device?"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Fuck It"
                                                    otherButtonTitles:nil];
            
            [message show];
            [self cycleTheGlobalMailComposer];
        }
        
    }else if([[notification name] isEqualToString:@"call"]){
        
        NSString *phoneNumber =  self.athlete.cellPhone;
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

-(void) didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:^
     { [self cycleTheGlobalMailComposer]; }
     ];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //if ([[segue identifier] isEqualToString:@"goToPayment"])
        //_paymentVC = [segue destinationViewController ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
