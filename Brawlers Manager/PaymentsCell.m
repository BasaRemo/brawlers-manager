//
//  PaymentsCell.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-26.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "PaymentsCell.h"
#import "VENSeparatorView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PaymentsCell ()
@end

@implementation PaymentsCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor redColor];
    //UIColorFromRGB(0xF9F9F5)
    [self.paymentView setTopLineSeparatorType:VENSeparatorTypeStraight
                        bottomLineSeparatorType:VENSeparatorTypeJagged];
    
    self.paymentView.fillColor         = UIColorFromRGB(0xF9F9F5);
    self.paymentView.topStrokeColor    = [UIColor blackColor];
    self.paymentView.bottomStrokeColor = [UIColor lightGrayColor];
    
    self.paymentView.jaggedEdgeVerticalVertexDistance = 7;
    self.paymentView.jaggedEdgeHorizontalVertexDistance = 7;
    
    self.paidBtn.layer.cornerRadius = 6.0f;
    self.paidBtn.backgroundColor = [UIColor nephritisColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
