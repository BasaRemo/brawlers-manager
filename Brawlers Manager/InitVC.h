//
//  InitVC.h
//  Brawlers Manager
//
//  Created by Professional on 2015-04-28.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+FlatUI.h"
#import "AthletesVC.h"
#import "HistoryVC.h"
#import "StatisticsVC.h"
#import "StudentCollectionViewController.h"

@interface InitVC : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
