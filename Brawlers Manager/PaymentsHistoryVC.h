//
//  PaymentsHistoryVC.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-23.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentsCell.h"
#import <Parse/Parse.h>

@interface PaymentsHistoryVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *paymentArray;
@property NSString *athleteName;

-(void) queryFromLocalStore;
@end
