//
//  StudentsCell.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-03.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"

@interface StudentsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *studentPic;
@property (weak, nonatomic) IBOutlet FUIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UIImageView *attendanceBtn;

-(void) setUserImageAndText:(NSString *)name;

@end
