//
//  HistoryVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "HistoryVC.h"

@interface HistoryVC ()

@end

@implementation HistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setPaymentHistory];
    //[[UINavigationBar appearance] setBarTintColor:[UIColor redColor]];
    [self.navigationItem setTitle:@"History"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(void) setPaymentHistory{
    _paymentHistoryVC= [self.storyboard instantiateViewControllerWithIdentifier:@"paymentHistoryVC"];
    
    CGRect frame = _paymentHistoryVC.view.frame;
    frame.origin.y = 43;
    _paymentHistoryVC.view.frame = frame;
    
    [self.view addSubview:_paymentHistoryVC.view];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
