//
//  PaymentsHistoryVC.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-23.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "PaymentsHistoryVC.h"
#import "VENSeparatorTableViewCellProvider.h"
#import "UIColor+FlatUI.h"
#import "AFNetworkReachabilityManager.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1]

@interface PaymentsHistoryVC () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) VENSeparatorTableViewCellProvider *separatorProvider;
@end

@implementation PaymentsHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpPaymentHistoryTableView];
    [self isInternetReachable];
}

- (void)setUpPaymentHistoryTableView
{
    self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.separatorProvider = [[VENSeparatorTableViewCellProvider alloc] initWithStrokeColor:[UIColor grayColor]
                                                                                  fillColor:[UIColor redColor]
                                                                                   delegate:self];

}

#pragma mark -
#pragma mark UITableView Delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    //view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    view.backgroundColor = UIColorFromRGB(0xC0392B);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(110, 4, 0, 0)];
    label.text = @"Payments history";
    label.font = [UIFont systemFontOfSize:16];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    return nil;
}
-(void) queryFromCache{
 
     PFQuery *query = [PFQuery queryWithClassName:@"Payment"];
     [query fromLocalDatastore];
    if (_athleteName) {
        [query whereKey:@"athleteName" equalTo:_athleteName];
    }
     [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
     if (!error) {
         _paymentArray = [[NSMutableArray alloc] initWithArray:objects];
         [_tableView reloadData];
     
     }
     }];
 }

-(BOOL) isInternetReachable
{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSLog(@"Reachability changed: %@", AFStringFromNetworkReachabilityStatus(status));
        
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                // -- Reachable -- //
                NSLog(@"Reachable");
                [self queryFromParse];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                // -- Not reachable -- //
                NSLog(@"Not Reachable");
                [self queryFromLocalStore];
                break;
        }
        
    }];
    return NO;
}
- (void) queryFromParse {
    
    PFQuery *query = [PFQuery queryWithClassName:@"Payment"];
    if (_athleteName) {
        [query whereKey:@"athleteName" equalTo:_athleteName];
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Online!");
            _paymentArray = [[NSMutableArray alloc] initWithArray:objects];
            [_tableView reloadData];
            [PFObject unpinAllObjectsInBackgroundWithName:@"Payment"];
            [PFObject pinAllInBackground:objects withName:@"Payment"];
            
        }
    }];

}

-(void) queryFromLocalStore{
    
    PFQuery *query = [PFQuery queryWithClassName:@"Payment"];
    [query fromLocalDatastore];
    if (_athleteName) {
        [query whereKey:@"athleteName" equalTo:_athleteName];
    }
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            _paymentArray = [[NSMutableArray alloc] initWithArray:objects];
            [_tableView reloadData];
            
        }
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex
{
    return 30;
}


/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 cell.backgroundColor = [UIColor clearColor];
 cell.textLabel.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
 cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
 }*/
/*-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 154;
}*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark -
#pragma mark UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return _paymentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"paymentCell";
    
    PaymentsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PaymentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    PFObject *tempObject = [_paymentArray objectAtIndex:indexPath.row];

    cell.subscription.text =[tempObject objectForKey:@"subscription"];
    cell.amount.text =[tempObject objectForKey:@"amount"];
    
    NSDate *today = tempObject.createdAt;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy HH:mm"];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSLog(@"Date: %@",dateString);
    cell.date.text = dateString;
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    //cell.textLabel.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
