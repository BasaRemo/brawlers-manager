//
//  StudentCollectionViewController.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-05.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "StudentCollectionViewController.h"
#import "UIImageView+Letters.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface StudentCollectionViewController ()
{
}
@end

@implementation StudentCollectionViewController

static NSString * const reuseIdentifier = @"Cell";


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    // Do any additional setup after loading the view.
    // Uncomment the following line to preserve selection between presentations

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    self.collectionView.allowsMultipleSelection = YES;
    self.collectionView.allowsSelection = YES;
    self.selectedStudents = [[NSMutableArray alloc] initWithObjects:nil];
    [self retrieveFromParse];
    [self setApparence];
}

- (void)setApparence
{
    //Nav Bar appearance
   // [self.totalNbView setBackgroundColor:[UIColor pomegranateColor]];
    //[self.totalNbView setAlpha:0.9];
    
    [self.navigationItem setTitle:@"Attendance"];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDoneButtonTouch:)];
    
    self.navigationItem.rightBarButtonItem = doneBtn;
    
    // Set round attendees counter
    //_attendentNb.layer.borderColor = [[UIColor greenColor] CGColor];
    //_attendentNb.layer.borderWidth = 3;
    //_attendentNb.layer.cornerRadius = 50.0;
}

- (void)onDoneButtonTouch:(UIBarButtonItem *)sender
{
    NSLog(@" Attendees: %@",_attendentNb.text);
}

- (void) retrieveFromParse {
    
    PFQuery *retrieveStudents = [PFQuery queryWithClassName:@"Athletes"];
    
    [retrieveStudents findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            studentsArray = [[NSMutableArray alloc] initWithArray:objects];
            NSLog(@"Objects Retrieved! %@",studentsArray);
            
        }
        [self.collectionView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [studentsArray count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AtheleteCollecCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    UIImageView *studentPic = (UIImageView *)[cell viewWithTag:100];
    UILabel *studentName = (UILabel*)[cell viewWithTag:101];
    UIImageView *attendanceBtn = (UIImageView *)[cell viewWithTag:102];
    NSLog(@"%@",studentName.text);
    PFObject *tempObject = [studentsArray objectAtIndex:indexPath.row];
    studentName.text = [NSString stringWithFormat: @"%@", [tempObject objectForKey:@"name"]];
    //[NSString stringWithFormat: @"%d", attendees];
    
    [cell setUserImageAndText:studentName.text];
    //[cell.layer setBorderWidth:2.0f];
    //[cell.layer setBorderColor:[UIColor blackColor].CGColor];
    //[cell.layer setCornerRadius:50.0f];
    
    

    
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AtheleteCollecCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImageView *attendanceBtn = (UIImageView *)[cell viewWithTag:102];
    [attendanceBtn setImage:[UIImage imageNamed:@"icon-checkbox-selected-green-25x25.png"]];
    
    // Update the Attendee number in the top view
    int attendees = [_attendentNb.text intValue];
    attendees++;
    _attendentNb.text =[NSString stringWithFormat: @"%d", attendees];
}

-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    AtheleteCollecCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImageView *attendanceBtn = (UIImageView *)[cell viewWithTag:102];
    [attendanceBtn setImage:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"]];
    
    // Update the Attendee number in the top view
    int attendees = [_attendentNb.text intValue];
    attendees--;
    _attendentNb.text =[NSString stringWithFormat: @"%d", attendees];
}

/*
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    //set color with animation
    [UIView animateWithDuration:0.1 delay:0 options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor colorWithRed:232/255.0f green:232/255.0f blue:232/255.0f alpha:1]];
                     }
                     completion:nil];
}

- (void)collectionView:(UICollectionView *)colView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    //set color with animation
    [UIView animateWithDuration:0.1
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor clearColor]];
                     }
                     completion:nil ];
}
*/
#pragma mark <UICollectionViewDelegate>
/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return YES;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
 */

@end
