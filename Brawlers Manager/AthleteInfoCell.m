//
//  AthleteInfoCell.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AthleteInfoCell.h"

@implementation AthleteInfoCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)contactBtnTapped:(id)sender {
    
    if ([self.title.text isEqualToString:@"Email"])
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendEmail" object:nil];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"call" object:nil];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
