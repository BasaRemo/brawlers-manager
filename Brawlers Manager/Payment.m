//
//  Payment.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-16.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "Payment.h"
#import <Parse/PFObject+Subclass.h>

@implementation Payment

@dynamic amount;
//@dynamic subscription;
//@dynamic complete;
+ (NSString *)parseClassName {
    return @"Payment";
}
@end