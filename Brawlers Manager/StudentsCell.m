//
//  StudentsCell.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-03.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "StudentsCell.h"
#import "UIImageView+Letters.h"

@implementation StudentsCell

- (void)awakeFromNib {
    // Initialization code
    //[self setUserImageAndText:@"Test"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    UIView *bgColorView = [[UIView alloc] init];
     bgColorView.backgroundColor = [UIColor clearColor];
     [self setSelectedBackgroundView:bgColorView];

    //button style
    //_statusBtn.buttonColor = [UIColor turquoiseColor];
    _statusBtn.cornerRadius = _statusBtn.frame.size.width/2;
}


-(void) setUserImageAndText:(NSString *) name{
    
    [_studentPic setImageWithString:name];
    
    // Set corner radius
    _studentPic.layer.cornerRadius = CGRectGetHeight(_studentPic.frame) / 2;
    _studentPic.clipsToBounds = YES;
    
    // Add border
    //_studentPic.layer.borderColor = [[UIColor colorWithWhite:0.84f alpha:1.0f] CGColor];
    //_studentPic.layer.borderWidth = 3.0f;
    
    // Add shadow
    _studentPic.layer.shadowOffset = CGSizeMake(0, 0);
    _studentPic.layer.shadowRadius = 1.5;
    _studentPic.layer.shadowOpacity = 0.3;
}
@end
