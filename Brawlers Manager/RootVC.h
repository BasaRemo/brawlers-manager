//
//  RootVC.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-06.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+FlatUI.h"


@interface RootVC:UIViewController
//@interface RootVC : REFrostedViewController
- (IBAction)showMenu:(id)sender;

@end
