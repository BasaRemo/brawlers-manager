//
//  AthleteInfoVC.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-27.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AthleteInfoVC.h"
#import "AthleteInfoCell.h"

@interface AthleteInfoVC ()

@end

@implementation AthleteInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpContactsInfoTableView];
}

/********** ATHLETE CONTACT INFO ************/

- (void)setUpContactsInfoTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 340)];
    self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
}

#pragma mark UITableView Delegate


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Separator line use full width
    /*if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
     [tableView setSeparatorInset:UIEdgeInsetsZero];
     }
     
     if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
     [tableView setLayoutMargins:UIEdgeInsetsZero];
     }
     
     if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
     [cell setLayoutMargins:UIEdgeInsetsZero];
     }*/
    cell.backgroundColor = [UIColor whiteColor];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    UIView *view;
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    view.backgroundColor = [UIColor cloudsColor];
    //view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    label.text = @"Contact Information";
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor midnightBlueColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    
    switch (sectionIndex) {
        case 0:
            label.text = @"Contact Information";
            break;
        case 1:
            label.text = @"Subscription State";
            break;
        case 2:
            label.text = @"Competition";
            break;
        default:
            break;
    }
    
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AthleteInfoCell";
    AthleteInfoCell *cell =  (AthleteInfoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AthleteInfoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.contactBtn.backgroundColor = [UIColor whiteColor];
    if (indexPath.section == 0) {
        
        NSArray *titles = @[@"Cell", @"Email", @"Facebook"];
        //NSLog(@"Athlele mail: %@",self.athlete.email);
        //NSArray *details = @[self.athlete.cellPhone, self.athlete.email, @"facebook.com/Athletes"];
        NSArray *details = @[@"514-553-6044", @"email@email.com", @"facebook.com/Athletes"];
        cell.title.text = titles[indexPath.row];
        cell.detail.text = details[indexPath.row];
        
        switch (indexPath.row) {
            case 0:
                [cell.contactBtn setImage:[UIImage imageNamed:@"phone1-50.png"] forState:UIControlStateNormal];
                break;
            case 1:
                [cell.contactBtn setImage:[UIImage imageNamed:@"message-50.png"] forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
    } else if(indexPath.section == 1){
        
        NSArray *titles = @[@"Subscription Expiration", @"Notes"];
        NSArray *details = @[@"March 6th 2015", @"tell me"];
        cell.contactBtn.hidden = YES;
        
        if (indexPath.row == 0) {
            
            cell.title.text = titles[indexPath.row];
            cell.detail.text = details[indexPath.row];
            
        }else{
            cell.contactBtn.hidden = YES;
            cell.title.text = titles[indexPath.row];
            cell.detail.text = @"";
            UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(cell.detail.frame.origin.x,cell.detail.frame.origin.y, cell.frame.size.width-30, 110)];
            //textView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            textView.text = details[indexPath.row];
            
            textView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
            textView.layer.borderWidth=1.0;
            textView.layer.cornerRadius = 8.0;
            textView.layer.masksToBounds = YES;
            
            [cell.contentView addSubview:textView];
            
        }
        
    }else if (indexPath.section == 2){
        
        cell.contactBtn.hidden = YES;
        NSArray *titles = @[@"Record", @"Next Fight"];
        NSArray *details = @[@"10-6", @"2015-04-21"];
        cell.title.text = titles[indexPath.row];
        cell.detail.text = details[indexPath.row];
    }
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    switch (sectionIndex) {
        case 0:
            return 3;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 2;
            break;
        default:
            break;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return 0;
    return 34;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 1)
        if (indexPath.row ==1)
            return 150;
    
    return 68;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
