//
//  RootVC.m
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-06.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "RootVC.h"
#import "AppDelegate.h"
#import <ParseUI/ParseUI.h>

@interface RootVC ()<PFLogInViewControllerDelegate>

@end

@implementation RootVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) setLogin{
    PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
    
    /*logInController.fields = (PFLogInFieldsUsernameAndPassword
                              |PFSignUpFieldsSignUpButton | PFLogInFieldsFacebook
                              | PFLogInFieldsTwitter);*/
    
   // [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"menuController"]];
   // [self setCenterPanel:logInController];
    
    //[self presentViewController:logInController animated:YES completion:nil];
}

- (void)logInViewController:(PFLogInViewController *)controller
               didLogInUser:(PFUser *)user {
    /*[self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"navController"]];
    [self dismissViewControllerAnimated:YES completion:nil];*/
}

- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
   // [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)awakeFromNib
{
    /*self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navController"];
    self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];*/
    
    //JASidePanel
    /*[self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"menuController"]];
    [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"navController"]];
    //[self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"contentController"]];
    self.leftGapPercentage = 0.78;*/
    [self setNotifications];
    //[self setLogin];
  
}

// Notifications to change Background
-(void) setNotifications {
    
    // Set Observer to change background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMenu:)name:@"showMenu" object:nil];
}

- (IBAction)showMenu: (NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"showMenu"]) {
        
        //[self presentMenuViewController];
        //JASidePanel
       // [self toggleLeftPanel:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
