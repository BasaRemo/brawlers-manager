//
//  Athlete.m
//  Brawlers Manager
//
//  Created by Professional on 2015-04-11.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "Athletes.h"

@implementation Athletes
+ (void)load {
    [self registerSubclass];
}
@dynamic name;
@dynamic age;
@dynamic sex;
@dynamic cellPhone;
@dynamic email;
@dynamic attendance;
@dynamic status;
@dynamic facebook;
+ (NSString *)parseClassName {
    return @"Athletes";
}

- (id)copyWithZone:(NSZone*)zone{
    
    Athletes *copy = [[[self class] allocWithZone:zone] init];
    copy.name = self.name;
    copy.email = self.email;
    copy.cellPhone = self.cellPhone;
    copy.age = self.age;
    copy.status = self.status;
    copy.attendance = self.attendance;
    return copy;
}
@end
