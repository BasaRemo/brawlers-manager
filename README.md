# BRAWLERS MANAGER #

### What? ###

* Brawlers manager is an IOS app for trainers to help manage athletes.
* The project started because I wanted to help my MMA and Boxing coach who was still using paper to take attendance and manage his athletes payments. 

* Current Version: 1.0

***** Screenshots *****

![1.png](https://bitbucket.org/repo/EzXxpG/images/2559360193-1.png)

![2.png](https://bitbucket.org/repo/EzXxpG/images/2833179228-2.png)

![3.png](https://bitbucket.org/repo/EzXxpG/images/3929237054-3.png)

![4.png](https://bitbucket.org/repo/EzXxpG/images/12773181-4.png)

![5.png](https://bitbucket.org/repo/EzXxpG/images/3707105843-5.png)

![6.png](https://bitbucket.org/repo/EzXxpG/images/2718613259-6.png)

### TODO ###

* Finish implementation of payment history page
* Implement user profile Edit View
* Implement Add payment View
* Add online payment 
* [Integrate userVoice](https://www.uservoice.com/)
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### CONTRIBUTOR ###

Ntambwa Basambombo